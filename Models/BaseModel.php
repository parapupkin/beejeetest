<?php
namespace Beejeetest\Models;

use PDO;


class BaseModel
{
    public $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
    }
}