<?php
namespace Beejeetest\Models;

/**
 * The user class provides login, creation and verification of user existence.
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class User extends BaseModel
{
    public function login($name, $password) {
        $sql= "SELECT * FROM users WHERE name = :name AND password = SHA2(password(:password), 512) AND is_admin=1 LIMIT 1";
        $query = $this->db->prepare($sql);
        $query->execute(array(
            ':name'    => $name,
            ':password' => $password
        ));

        return $query->fetch();
    }

    public function createUser($name, $email, $password=null, $is_admin=0) {
        $sql = 'INSERT INTO users (name, email, password, is_admin) VALUES (:name, :email, :password, :is_admin)';
        $query = $this->db->prepare($sql);

        $query->execute(array(
            ':name' => $name,
            ':email'    => $email,
            ':password' => ($password ? password_hash($password) : null),
            ':is_admin' => $is_admin
        ));

        return $this->db->lastInsertId();
    }

    public function checkIsset($name, $email) {
        $sql = 'SELECT * FROM users WHERE name= :name and email= :email';
        $query = $this->db->prepare($sql);
        $query->execute(array(
            ':name' => $name,
            ':email' => $email
        ));
        $arrayUsers = $query->fetchAll();
        if(count($arrayUsers) !==0) {
            return $arrayUsers[0]['id'];
        } else {
            return null;
        }
    }
}