<?php
namespace Beejeetest\Models;

/**
 * The Task class provides CRUD operations on tasks.
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class Task extends BaseModel
{
    public function getAllTasks() {
        $paramSort = 'name';
        $order = 'asc';
        if($_SESSION['paramSort'] && $_SESSION['order']) {
            if(array_search($_SESSION['paramSort'], COLUMNS_SORT) !== false) {
                $paramSort = $_SESSION['paramSort'];
            }
            if($_SESSION['order'] === 'desc') {
                $order = $_SESSION['order'];
            }
        }
        $query = $this->db->query('SELECT * FROM tasks, users WHERE tasks.user_id=users.user_id order by ' . $paramSort . ' ' . $order );

        return $query->fetchAll();
    }

    public function getTask($id) {
        $sql = 'SELECT * FROM tasks  left join users on tasks.user_id=users.user_id WHERE tasks.id=:id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':id' => $id
        ]);

        return $query->fetch();

    }

    public function store($user_id, $description) {
        $sql = 'INSERT INTO tasks (user_id,  description) VALUES ( :user_id, :description)';
        $query = $this->db->prepare($sql);

        return $query->execute([
            ':user_id' => $user_id,
            ':description' => $description
        ]);
    }

    public function update($taskId, $status, $description, $editedAdmin) {
        $sql = 'UPDATE tasks SET status=:status, description=:description, edited_admin=:edited_admin WHERE id=:taskId';
        $query = $this->db->prepare($sql);

        return $query->execute([
            ':status' => $status,
            ':description' => $description,
            ':taskId' => $taskId,
            ':edited_admin' => $editedAdmin
        ]);
    }
}