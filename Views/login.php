<div class="container-fluid login-form-container">
    <div class="row justify-content-center align-items-center ht-100">
        <div class="login-container">
            <div class="card login-form">
                <div class="card-header mt-2">
                    Авторизация
                </div>
                <div class="card-body">
                    <form method="POST" action="/user/checkUser" novalidate>
                        <?php if(isset($_SESSION['errors']['login'])) { ?>
                        <span class="error"><?php echo $_SESSION['errors']['login'] ?></span>
                        <?php } ?>
                        <div class="form-group">
                            <div>
                                <input id="login" type="text" class="form-control border-input " name="login" placeholder="Login" value="" required="" autocomplete="login" autofocus="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <input id="password" type="password" class="form-control border-input  " placeholder="Password" name="password" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-login">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
