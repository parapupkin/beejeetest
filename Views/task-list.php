<div class="card-body register-table-index">
    <section class="register-table">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-xl-12 table-responsive">
                <div id="example-table"></div>
                <table class="table table-striped -table-bordered table-borderless">
                    <thead class="text-left">
                    <tr class="register-head-row text-nowrap">
                        <th data-column-name="user-name">
                            <div class="container-fluid">
                                <div class="row flex-nowrap">
                                    <div class="col item sort-title text-left">
                                        <span>User Name</span>
                                    </div>
                                    <div class="col item sorting-list-arrows text-right pl-2">
                                        <?php if($_SESSION['paramSort'] !== 'name') { ?>
                                            <a href="/system/setSort?paramSort=name&order=asc" class="fa fa-exchange-alt  fa-rotate-90 register-icon-sort"></a>
                                        <?php } else if ($_SESSION['order'] === 'asc') { ?>
                                            <a href="/system/setSort?paramSort=name&order=desc" class="fa fa-long-arrow-alt-up"></a>
                                        <?php } else { ?>
                                            <a href="/system/setSort?paramSort=name&order=asc" class="fa fa-long-arrow-alt-down"></a>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th data-column-name="email">
                            <div class="container-fluid">
                                <div class="row flex-nowrap">
                                    <div class="col item sort-title text-left">
                                        <span>Email</span>
                                    </div>
                                    <div class="col item sorting-list-arrows text-right pl-2">
                                        <?php if($_SESSION['paramSort'] !== 'email') { ?>
                                            <a href="/system/setSort?paramSort=email&order=asc" class="fa fa-exchange-alt  fa-rotate-90 register-icon-sort"></a>
                                        <?php } else if ($_SESSION['order'] === 'asc') { ?>
                                            <a href="/system/setSort?paramSort=email&order=desc" class="fa fa-long-arrow-alt-up"></a>
                                        <?php } else { ?>
                                            <a href="/system/setSort?paramSort=email&order=asc" class="fa fa-long-arrow-alt-down"></a>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th data-column-name="description">
                            <div class="container-fluid">
                                <div class="row flex-nowrap">
                                    <div class="col item sort-title text-left">
                                        <span>Description</span>
                                    </div>
                                    <div class="col item sorting-list-arrows text-right pl-2">
                                        <?php if($_SESSION['paramSort'] !== 'description') { ?>
                                            <a href="/system/setSort?paramSort=description&order=asc" class="fa fa-exchange-alt  fa-rotate-90 register-icon-sort"></a>
                                        <?php } else if ($_SESSION['order'] === 'asc') { ?>
                                            <a href="/system/setSort?paramSort=description&order=desc" class="fa fa-long-arrow-alt-up"></a>
                                        <?php } else { ?>
                                            <a href="/system/setSort?paramSort=description&order=asc" class="fa fa-long-arrow-alt-down"></a>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th data-column-name="status">
                            <div class="container-fluid">
                                <div class="row flex-nowrap">
                                    <div class="col item sort-title text-left">
                                        <span>Status</span>
                                    </div>
                                    <div class="col item sorting-list-arrows text-right pl-2">
                                        <?php if($_SESSION['paramSort'] !== 'status') { ?>
                                            <a href="/system/setSort?paramSort=status&order=asc" class="fa fa-exchange-alt  fa-rotate-90 register-icon-sort"></a>
                                        <?php } else if ($_SESSION['order'] === 'asc') { ?>
                                            <a href="/system/setSort?paramSort=status&order=desc" class="fa fa-long-arrow-alt-up"></a>
                                        <?php } else { ?>
                                            <a href="/system/setSort?paramSort=status&order=asc" class="fa fa-long-arrow-alt-down"></a>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <?php if($_SESSION['admin']) { ?>
                        <th data-column-name="edited_admin">
                            <div class="container-fluid">
                                <div class="row flex-nowrap">
                                    <div class="col item sort-title text-left">
                                        <span>Edited admin</span>
                                    </div>
                                    <div class="col item sorting-list-arrows text-right pl-2">
                                        <?php if($_SESSION['paramSort'] !== 'edited_admin') { ?>
                                            <a href="/system/setSort?paramSort=edited_admin&order=asc" class="fa fa-exchange-alt  fa-rotate-90 register-icon-sort"></a>
                                        <?php } else if ($_SESSION['order'] === 'asc') { ?>
                                            <a href="/system/setSort?paramSort=edited_admin&order=desc" class="fa fa-long-arrow-alt-up"></a>
                                        <?php } else { ?>
                                            <a href="/system/setSort?paramSort=edited_admin&order=asc" class="fa fa-long-arrow-alt-down"></a>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th data-column-name="actions">
                            <div>
                                <div class="float-left sort-title">
                                    <span>Actions</span>
                                </div>
                            </div>
                        </th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i=$page*3-3; $i<$page*3; $i++) { ?>
                        <?php if(isset($tasks[$i])) { ?>
                        <tr class="register-row">
                            <td>
                                <?php echo htmlspecialchars($tasks[$i]['name']); ?>
                            </td>
                            <td>
                                <?php echo htmlspecialchars($tasks[$i]['email']); ?>
                            </td>
                            <td>
                                <?php echo htmlspecialchars($tasks[$i]['description']) ; ?>
                            </td>
                            <td>
                                <?php echo htmlspecialchars($tasks[$i]['status']) ?>
                            </td>
                            <?php if($_SESSION['admin']) { ?>
                            <td>
                                <?php echo htmlspecialchars($tasks[$i]['edited_admin']) ?>
                            </td>
                            <td class="active-icon">
                                <div class="form-wrapper">
                                    <form action="/task/edit" class="d-inline" method="post">
                                        <input type="hidden" name="taskId" value="<?php echo $tasks[$i]['id'] ?? ''; ?>">
                                        <button type="submit" class="btn btn-sm ">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item <?php echo $page==1 ? 'active' : ''; ?>"><a class="page-link" href="/system/setPage?page=1">1</a></li>
                        <?php for ($i=3; $i<count($tasks); $i+=3) { ?>
                            <li class="page-item <?php echo $page==$i/3 + 1 ? 'active' : ''; ?>"><a class="page-link" href="/system/setPage?page=<?php echo $i/3 + 1?>"><?php echo $i/3 + 1?></a></li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
</div>

