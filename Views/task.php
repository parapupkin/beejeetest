<div class="container-fluid py-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card text-left">
                <div class="card-header">
                    <h5 class="float-left">Task info</h5>
                </div>
                <?php if(isset($title) && ($title === 'taskCreate')) { ?>
                <form id="create-task-form" action="/task/store" method="post" novalidate="" >
                <?php } else if (isset($title) && ($title === 'taskEdit')) { ?>
                <form id="create-task-form" action="/task/update" method="post" novalidate="" >
                    <input type="hidden" name="id" value="<?php echo $task['id'] ?? ''; ?>">
                <?php } ?>
                    <div class="card-body">
                        <div class="mb-3 d-none">
                            <span class="error mb-3"></span>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Name*</span>
                            </div>
                            <input type="text" class="form-control" aria-label="Name" name="name" maxlength="50" minlength="2" <?php if(isset($task['name'])) {echo 'disabled value=' . $task['name'];}; ?> required>
                        </div>
                        <div class="mb-3 d-none">
                            <span class="error mb-3"></span>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Email*</span>
                            </div>
                            <input type="email" class="form-control" aria-label="Email" aria-describedby="basic-addon1" required name="email" maxlength="100" data-error-message="Enter correct email" <?php if(isset($task['email'])) {echo 'disabled value=' . $task['email'];}; ?>>
                        </div>

                        <div class="mb-3 d-none">
                            <span class="error mb-3"></span>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Description*</span>
                            </div>
                            <textarea class=" form-control" aria-label="With textarea" name="description" maxlength="300" minlength="3" required=""><?php echo $task['description'] ?? ''; ?></textarea>
                        </div>
                        <?php if(isset($title) && ($title === 'taskEdit')) { ?>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck" name="status" <?php if(isset($task['status']) && $task['status'] === 'Completed') {echo 'checked';}; ?>>
                                <label class="form-check-label" for="gridCheck">
                                    Completed
                                </label>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="mb-3">
                            <span class="success d-none">
                                Sending data to the server
                            </span>
                        </div>
                        <section class="btn-group" role="group" aria-label="Basic example">
                            <button type="submit" class="btn save">Save</button>
                            <button type="button" class="btn exit ml-3">Go to task list</button>
                        </section>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
