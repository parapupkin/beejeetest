<header class="app-header">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container-fluid pr-0">
            <a class="navbar-brand nav-logo" href="#">
                BeeJeeTest
            </a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item  <?php  echo (isset($title) && $title === 'tasks') ? (('active')) : '' ?> ">
                        <a class="nav-link " href="/">Task list</a>
                    </li>
                    <li class="nav-item <?php  echo (isset($title) && $title === 'addTask') ? (('active')) : '' ?>">
                        <a class="nav-link " href="/task/create">Add task</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <?php  if (!$_SESSION['admin']) { ?>
                    <li class="nav-item">
                        <a class="nav-link " href="/user/login">Login</a>
                    </li>
                    <?php  } else { ?>
                    <li class="nav-item">
                        <a class="nav-link " href="/user/logout">Logout</a>
                    </li>
                    <?php  } ?>
                </ul>
            </div>
        </div>
    </nav>
</header>