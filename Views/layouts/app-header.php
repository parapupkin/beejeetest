<!DOCTYPE html>
<html lang="en">
<head>
    <title>BeeJeeTest</title>
    <link rel="stylesheet" href="/public/css/login-form.css">
    <link rel="stylesheet" href="/public/css/main.css">
    <link rel="stylesheet" href="/public/css/fontawesome-free-5.8.2-web/css/all.css">
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
</head>
<body>
<?php if(isset($title) && ($title === 'tasks' || $title === 'taskCreate' || $title === 'taskEdit')) {
    require_once realpath($_SERVER["DOCUMENT_ROOT"] . '/Views/layouts/header.php');
} ?>
<div class="main-content jumbotron-fluid">

