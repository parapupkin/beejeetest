        </div>
        <script type="text/javascript" src="/public/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="/public/js/popper.js"></script>
        <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
        <?php if(isset($title) && ($title === 'taskCreate' || $title === 'taskEdit')) { ?>
            <script type="text/javascript" src="/public/js/validation.js"></script>
        <?php } ?>
    </body>
</html>
