<?php
/** Beejeetest base path */
define('BASE_PATH', 'http://beejeetest/');
// db-connection
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'beejee_test');
// list of columns by which you can sort
define('COLUMNS_SORT', [
    'name',
    'email',
    'description',
    'status',
    'edited_admin'
]);