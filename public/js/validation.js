// functions for showing, hiding error messages and processing form input fields
'use strict';

function checkValidity(elem) {
	let errorMessage = '';

	if (elem.dataset.errorMessage) {
		if (!elem.validity.valid) {
			errorMessage = elem.dataset.errorMessage;
		}
	} else {
		if (elem.validity.tooShort) {
			errorMessage = `Less than ${elem.minLength} characters allowed`;
		}

		if (elem.validity.tooLong) {
			errorMessage = `Length more than ${elem.maxLength} characters allowed`;
		}
	}

	if (elem.validity.valueMissing && !elem.dataset.errorMessage) {
		errorMessage = 'Required field';
	}

	if (!elem.validity.valid) {
		showErrorMessage(elem, errorMessage);
		possibilitySendForm = false;
	}
}

function showErrorMessage(checkedItem, errorMessage) {
	const itemError = checkedItem.parentNode.previousElementSibling;
	if (itemError) {
		const spanErrorMessage = itemError.querySelector('.error');
		if (spanErrorMessage) {
			itemError.classList.remove('d-none');
			checkedItem.parentNode.classList.add('invalid');
			spanErrorMessage.innerText = errorMessage;
		}
	}
}

function hideErrorMessage(checkedItem) {
	const itemError = checkedItem.parentNode.previousElementSibling;
	if (itemError) {
		const spanErrorMessage = itemError.querySelector('.error');
		if (spanErrorMessage) {
			itemError.classList.add('d-none');
			checkedItem.parentNode.classList.remove('invalid');
			spanErrorMessage.innerText = '';
		}
	}
}

const success = document.querySelector('.success'),
	  btnSave = document.querySelector('.save'),	 
	  btnExit = document.querySelector('.exit'),	  // 
	  form = document.getElementById('create-task-form'),
	  description = document.querySelector('textarea[name="description"]'),
	  inputs = form.querySelectorAll('input');

let possibilitySendForm = true;

function validate(event) {
	event.preventDefault();	
    possibilitySendForm = true;
    
    // validation of inputs
    for (const input of inputs) {
    	hideErrorMessage(input);
    	checkValidity(input);    	
    }

    // validation textarea
	hideErrorMessage(description);
	checkValidity(description);
	// if all fields are filled in correctly - send data to the server
    if (possibilitySendForm) {
    	btnSave.setAttribute('disabled', 'disabled');
    	const formData = new FormData(form);		
		fetch(form.action, {method: 'POST', body: formData})
	        .then(res => {
	        	if (res.status !== 200) {
	        		throw new TypeError('An error occurred on the server while saving the task');
	        	}	        		            
	            return res.json();          
	        })
	        .then(data => {
				if (data.unauthorized) {
					location.href = '/user/login';
				} else {
					success.classList.remove('d-none');
					if(data.success) {
						success.classList.add('success');
						success.classList.remove('error');
						success.innerText = data.success;
					} else {
						success.classList.add('error');
						success.classList.remove('success');
						success.innerText = data.error;
					}
				}
	        })
	        .catch(() => {	        	
	        	success.classList.remove('success');
	        	success.classList.remove('d-none');
	        	success.classList.add('error');
	        	success.innerText = 'An error occurred while saving the task';
	        });	
    } else {
    	success.classList.remove('success');
    	success.classList.remove('d-none');
    	success.classList.add('error');
    	success.innerText = 'Сheck that all fields are filled in correctly!';
    }
    	
}

form.addEventListener('submit', validate);

btnExit.addEventListener('click', () => {
	location.href = '/task/index';
});