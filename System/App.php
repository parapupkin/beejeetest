<?php
namespace Beejeetest\System;

/**
 * The App class provides application launch and serves as a router.
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class App
{
    public static function run() {
         $path = $_SERVER['REQUEST_URI'];
         $pathParts = explode('/', $path);
         $controller = $pathParts[1] ? ucfirst($pathParts[1]) : 'Task';
         $action = $pathParts[2] ? explode('?', $pathParts[2])[0] : 'index';
         $controllerNameSpace = 'Beejeetest\Controllers\\' . $controller . 'Controller';
         if (!class_exists($controllerNameSpace)) {
             throw new \ErrorException( $controllerNameSpace . ' Controller does not exist');
         }
         $objController = new $controllerNameSpace;
         if (!method_exists($objController, $action)) {
             throw new \ErrorException($action . ' action does not exist');
         }

         $objController->$action();
         exit();
    }
}