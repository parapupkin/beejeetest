<?php
namespace Beejeetest\System;

/**
 * The View class renders html pages to the user
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class View
{
    public function view($view, $data = []) {
        if (isset($data) && $data) extract($data);
        ob_start();
        require_once realpath($_SERVER["DOCUMENT_ROOT"] . '/Views/layouts/app-header.php');
        require_once realpath($_SERVER["DOCUMENT_ROOT"] . '/Views/' . $view . '.php');
        require_once realpath($_SERVER["DOCUMENT_ROOT"] . '/Views/layouts/app-footer.php');
        $buffer = ob_get_contents();
        @ob_end_clean();
        echo $buffer;
    }
}
