<?php
namespace Beejeetest\Controllers;

use Beejeetest\Models\Task;
use Beejeetest\Models\User;
use Beejeetest\System\View;

/**
 * The TaskController class provides CRUD operations on tasks.
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class TaskController
{
    public function index() {
        $render = new View;
        $task = new Task;
        $tasks = $task->getAllTasks();
        $page = $_SESSION['page'] ? : 1;
        $render->view('task-list', ['title' => 'tasks', 'page' => $page, 'tasks' => $tasks]);
    }

    public function create() {
        $render = new View;
        $render->view('task', ['title' => 'taskCreate']);
    }

    public function edit() {
        if(!$_SESSION['admin']) {
            header("Location: " . BASE_PATH . 'user/login' );
            die;
        }
        $render = new View;
        $task = new Task;
        $id = $_POST['taskId'];
        $task = $task->getTask($id);
        $render->view('task', ['title' => 'taskEdit', 'task' => $task]);
    }

    public function update() {
        header('Content-Type: application/json; charset=windows-1251');
        if(!$_SESSION['admin']) {
            echo json_encode(['unauthorized' => 'An error occurred while saving the task']);
            die;
        }
        $taskId = $_POST['id'];
        $task = new Task;
        $task = $task->getTask($taskId);
        $oldDescription = $task['description'];
        $editedAdmin = $task['edited_admin'];
        $description = htmlspecialchars($_POST['description']);
        if(strcmp($oldDescription, $description) !== 0) {
            $editedAdmin = 'Edited admin';
        }
        $status = $_POST['status'];
        $status = ($status === 'on') ? 'Completed' : 'Not  completed';
        $task = new Task;
        $isUpdate = $task->update($taskId, $status, $description, $editedAdmin);
        if ($isUpdate) {
            echo json_encode(['success' => 'Task saved successfully']);
        } else {
            echo json_encode(['error' => 'An error occurred while saving the task']);
        }
    }

    public function store() {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $email = $_POST['email'];
        $task = new Task;
        $user = new User;
        // to avoid duplicate users
        $user_id = $user->checkIsset($name, $email);
        if(!$user_id) {
            $user_id = $user->createUser($name, $email);
        }
        $isStore = $task->store($user_id, $description);
        header('Content-Type: application/json; charset=windows-1251');
        if ($isStore) {
            echo json_encode(['success' => 'Task saved successfully']);
        } else {
            echo json_encode(['error' => 'An error occurred while saving the task']);
        }
    }
}