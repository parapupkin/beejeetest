<?php
namespace Beejeetest\Controllers;

use Beejeetest\Models\User;
use Beejeetest\System\View;
/**
 * The UserController provides login, logout user
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class UserController
{
    public function checkUser() {
        $_SESSION['errors']['login'] = null;
        $login = $_POST['login'];
        $password = $_POST['password'];
        if (!$login || !$password) {
            $_SESSION['errors']['login'] = 'Login and password fields are required';
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit();
        }
        $user = new User;
        $user = $user->login($login, $password);
        if($user) {
            $_SESSION['admin'] = $user['name'];
            header("Location: " . BASE_PATH );
            die;
        } else {
            $_SESSION['errors']['login'] = 'Invalid login or password';
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit();
        }
    }

    public function login() {
        $render = new View;
        $render->view('login', ['title' => 'login']);
    }

    public function logout() {
        unset($_SESSION['admin']);
        header("Location: " . BASE_PATH );
        exit();
    }
}