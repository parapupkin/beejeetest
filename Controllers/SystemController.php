<?php
namespace Beejeetest\Controllers;

/**
 * The SystemController class provides options for sorting and pagination.
 *
 * @author Vadim Ustinovich parapupkin@rambler.ru
 */
class SystemController
{
    public function setPage(){
        $page = $_GET['page'];
        $_SESSION['page'] = $page;
        header("Location: " . BASE_PATH );
        exit();
    }

    public function setSort(){
        $paramSort = $_GET['paramSort'];
        $order = $_GET['order'];
        $_SESSION['paramSort'] = $paramSort;
        $_SESSION['order'] = $order;
        header("Location: " . BASE_PATH );
        exit();
    }
}