# BeeJeeTest
---
The project was created on the basis of a BeeJee test assignment. Allows you to view a complete list of tasks stored in the database, sort them by various columns. In administrator mode, you can edit tasks and mark them as marked.

---

##Requirements

* PHP 7.1.3 or higher;

* PDO-SQLite PHP extension enabled;

---

##Install    


    git clone git@bitbucket.org:parapupkin/beejeetest.git    
    
* configure config.php

* execute sql scripts from db_install directory 



---

##Admin mode

To enter administrator mode, enter login admin and password 123 on the login page